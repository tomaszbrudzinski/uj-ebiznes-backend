-- noinspection SqlDialectInspectionForFile

# --- !Ups

CREATE TABLE "category" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" VARCHAR NOT NULL
);

CREATE TABLE "supplier" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" VARCHAR NOT NULL,
    "address" TEXT NOT NULL
);

CREATE TABLE "product" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" VARCHAR NOT NULL,
    "description" TEXT NOT NULL,
    "category" INT NOT NULL,
    FOREIGN KEY(category) references category(id)
);

CREATE TABLE "cart" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "product" INT NOT NULL,
    "user" VARCHAR NOT NULL,
    "discount" INTEGER NULLABLE,
    FOREIGN KEY (product) REFERENCES product(id)
);

CREATE TABLE "payment" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "amount" INT NOT NULL,
    "accountNumber" TEXT NOT NULL
);

CREATE TABLE "order" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "user" VARCHAR NOT NULL,
    "cart" INT NOT NULL,
    "payment" INT NOT NULL,
    FOREIGN KEY (cart) REFERENCES cart(id),
    FOREIGN KEY (payment) REFERENCES payment(id)
);

CREATE TABLE "discount" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "value" INTEGER NOT NULL,
    "active" INTEGER NOT NULL
);

CREATE TABLE "coupon" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "code" VARCHAR NOT NULL,
    "value" INTEGER NOT NULL,
    "active" INTEGER NOT NULL
);

CREATE TABLE "page" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "content" TEXT NOT NULL
);

# --- !Downs

DROP TABLE "category";
DROP TABLE "supplier";
DROP TABLE "cart";
DROP TABLE "product";
DROP TABLE "payment";
DROP TABLE "order";
DROP TABLE "discount";
DROP TABLE "coupon";
DROP TABLE "page";
