# --- !Ups
INSERT INTO "category"("name") VALUES("albums");
INSERT INTO "category"("name") VALUES("posters");
INSERT INTO "category"("name") VALUES("t-shirts");

INSERT INTO "product"("name", "description", "category") VALUES ("Elephant", "White Stripes", 1);
INSERT INTO "product"("name", "description", "category") VALUES ("Mother's Milk", "Red Hot Chilli Peppers", 1);
INSERT INTO "product"("name", "description", "category") VALUES ("Rage Against The Machine", "Rage Against The Machine", 1);
INSERT INTO "product"("name", "description", "category") VALUES ("Led Zeppelin IV", "Led Zeppelin", 1);
INSERT INTO "product"("name", "description", "category") VALUES ("Nevermind", "Nirvana", 2);
INSERT INTO "product"("name", "description", "category") VALUES ("Ten", "Pearl Jam", 2);
INSERT INTO "product"("name", "description", "category") VALUES ("Proud like a God", "KoRn", 3);
INSERT INTO "product"("name", "description", "category") VALUES ("Mutter", "Rammstein", 3);
INSERT INTO "product"("name", "description", "category") VALUES ("KoRn", "KoRn", 3);

INSERT INTO "supplier"("name", "address") VALUES ("Empik", "El. Empikowska 13, 12-345 Empikowo");

INSERT INTO "discount"("value", "active") VALUES (10, 1);

INSERT INTO "coupon"("code", "value", "active") VALUES ("WHITE_MONDAY", 5, 1);

INSERT INTO "page"("content") VALUES ("Terms and conditions...")

# --- !Downs
DELETE FROM "product";
DELETE FROM "category";
DELETE FROM "supplier";
DELETE FROM "discount";
DELETE FROM "coupon";
DELETE FROM "page";
