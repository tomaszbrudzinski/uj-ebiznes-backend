package models

import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class CartRepository @Inject() (dbConfigProvider: DatabaseConfigProvider, productRepository: ProductRepository)(implicit ec: ExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  val cart = TableQuery[CartTable]
  import productRepository.ProductTable

  private val prod = TableQuery[ProductTable]

  def create(product: Long, user: String, discount: Int): Future[Cart] = db.run {
    (cart.map(p => (p.product, p.user, p.discount))
      returning cart.map(_.id)
      into { case ((product, user, discount), id) => Cart(id, product, user, discount) }
    ) += (product, user, discount)
  }

  def list(): Future[Seq[Cart]] = db.run {
    cart.result
  }

  def getByUser(userID: String): Future[Seq[Cart]] = db.run {
    cart.filter(_.user === userID).result
  }

  def getById(id: Long): Future[Cart] = db.run {
    cart.filter(_.id === id).result.head
  }

  def getByIdOption(id: Long): Future[Option[Cart]] = db.run {
    cart.filter(_.id === id).result.headOption
  }

  def delete(id: Long): Future[Unit] = db.run(cart.filter(_.id === id).delete).map(_ => ())

  def update(id: Long, newCart: Cart): Future[Unit] = {
    val cartToUpdate: Cart = newCart.copy(id)
    db.run(cart.filter(_.id === id).update(cartToUpdate)).map(_ => ())
  }

  class CartTable(tag: Tag) extends Table[Cart](tag, "cart") {

    def product = column[Long]("product")

    def * = (id, product, user, discount) <> ((Cart.apply _).tupled, Cart.unapply)

    def user = column[String]("user")

    def discount = column[Int]("discount")

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  }

}

