package models

import play.api.libs.json.{ Json, OFormat }

case class Coupon(id: Long, code: String, value: Int, active: Int)

object Coupon {
  implicit val cartFormat: OFormat[Coupon] = Json.format[Coupon]
}
