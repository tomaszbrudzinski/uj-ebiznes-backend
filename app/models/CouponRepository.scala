package models

import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class CouponRepository @Inject() (dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  val coupon = TableQuery[CouponTable]

  def create(code: String, value: Int, active: Int): Future[Coupon] = db.run {
    (coupon.map(c => (c.code, c.value, c.active))
      returning coupon.map(_.id)
      into { case ((code, value, active), id) => Coupon(id, code, value, active) }
      ) += (code, value, active)
  }

  def list(): Future[Seq[Coupon]] = db.run {
    coupon.result
  }

  def getById(id: Long): Future[Coupon] = db.run {
    coupon.filter(_.id === id).result.head
  }

  def getByIdOption(id: Long): Future[Option[Coupon]] = db.run {
    coupon.filter(_.id === id).result.headOption
  }

  def delete(id: Long): Future[Unit] = db.run(coupon.filter(_.id === id).delete).map(_ => ())

  def update(id: Long, newCoupon: Coupon): Future[Unit] = {
    val couponToUpdate: Coupon = newCoupon.copy(id)
    db.run(coupon.filter(_.id === id).update(couponToUpdate)).map(_ => ())
  }

  class CouponTable(tag: Tag) extends Table[Coupon](tag, "coupon") {

    def * = (id, code, value, active) <> ((Coupon.apply _).tupled, Coupon.unapply)

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def code = column[String]("code")

    def value = column[Int]("value")

    def active = column[Int]("active")
  }

}

