package models

import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class OrderRepository @Inject() (dbConfigProvider: DatabaseConfigProvider, cartRepository: CartRepository, paymentRepository: PaymentRepository)(implicit ec: ExecutionContext) {
  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  private val order = TableQuery[OrderTable]

  import cartRepository.CartTable
  import paymentRepository.PaymentTable

  private val cartQuery = TableQuery[CartTable]
  private val paymentQuery = TableQuery[PaymentTable]

  def create(user: String, cart: Long, payment: Long): Future[Order] = db.run {
    (order.map(o => (o.user, o.cart, o.payment))
      returning order.map(_.id)
      into { case ((user, cart, payment), id) => Order(id, user, cart, payment) }
    ) += (user, cart, payment)
  }

  def list(): Future[Seq[Order]] = db.run {
    order.result
  }

  def getByUser(userID: String): Future[Seq[Order]] = db.run {
    order.filter(_.user === userID).result
  }

  def getById(id: Long): Future[Order] = db.run {
    order.filter(_.id === id).result.head
  }

  def getByIdOption(id: Long): Future[Option[Order]] = db.run {
    order.filter(_.id === id).result.headOption
  }

  def delete(id: Long): Future[Unit] = db.run(order.filter(_.id === id).delete).map(_ => ())

  def update(id: Long, newOrder: Order): Future[Unit] = {
    val orderToUpdate: Order = newOrder.copy(id)
    db.run(order.filter(_.id === id).update(orderToUpdate)).map(_ => ())
  }

  private class OrderTable(tag: Tag) extends Table[Order](tag, "order") {

    def cartFk = foreignKey("cart_fk", cart, cartQuery)(_.id)

    def paymentFk = foreignKey("payment_fk", payment, paymentQuery)(_.id)

    def * = (id, user, cart, payment) <> ((Order.apply _).tupled, Order.unapply)

    def user = column[String]("user")

    def cart = column[Long]("cart")

    def payment = column[Long]("payment")

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  }

}

