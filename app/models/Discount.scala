package models

import play.api.libs.json.{ Json, OFormat }

case class Discount(id: Long, value: Int, active: Int)

object Discount {
  implicit val cartFormat: OFormat[Discount] = Json.format[Discount]
}