package models

import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class ProductRepository @Inject() (dbConfigProvider: DatabaseConfigProvider, categoryRepository: CategoryRepository)(implicit exec: ExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  val product = TableQuery[ProductTable]

  import categoryRepository.CategoryTable

  private val cat = TableQuery[CategoryTable]

  def create(name: String, description: String, category: Long): Future[Product] = db.run {
    (product.map(p => (p.name, p.description, p.category))
      returning product.map(_.id)
      into { case ((name, description, category), id) => Product(id, name, description, category) }
    ) += (name, description, category)
  }

  def list(): Future[Seq[Product]] = db.run {
    product.result
  }

  def getByCategory(categoryID: Long): Future[Seq[Product]] = db.run {
    product.filter(_.category === categoryID).result
  }

  def getById(id: Long): Future[Product] = db.run {
    product.filter(_.id === id).result.head
  }

  def getByIdOption(id: Long): Future[Option[Product]] = db.run {
    product.filter(_.id === id).result.headOption
  }

  def getByCategories(categoryIDs: List[Long]): Future[Seq[Product]] = db.run {
    product.filter(_.category inSet categoryIDs).result
  }

  def delete(id: Long): Future[Unit] = db.run(product.filter(_.id === id).delete).map(_ => ())

  def update(id: Long, newProduct: Product): Future[Unit] = {
    val productToUpdate: Product = newProduct.copy(id)
    db.run(product.filter(_.id === id).update(productToUpdate)).map(_ => ())
  }

  class ProductTable(tag: Tag) extends Table[Product](tag, "product") {

    def * = (id, name, description, category) <> ((Product.apply _).tupled, Product.unapply)

    def category = column[Long]("category")

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def name = column[String]("name")

    def description = column[String]("description")

  }

}

