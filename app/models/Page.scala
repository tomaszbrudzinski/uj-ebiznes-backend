package models

import play.api.libs.json.{ Json, OFormat }

case class Page(id: Long, content: String)

object Page {
  implicit val productFormat: OFormat[Product] = Json.format[Product]
}