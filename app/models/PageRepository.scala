package models

import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class PageRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  val page = TableQuery[PageTable]

  def create(content: String): Future[Page] = db.run {
    (page.map(p => p.content)
      returning page.map(_.id)
      into { case (content, id) => Page(id, content) }
    ) += content
  }

  def list(): Future[Seq[Page]] = db.run {
    page.result
  }

  def getById(id: Long): Future[Page] = db.run {
    page.filter(_.id === id).result.head
  }

  def getByIdOption(id: Long): Future[Option[Page]] = db.run {
    page.filter(_.id === id).result.headOption
  }

  def delete(id: Long): Future[Unit] = db.run(page.filter(_.id === id).delete).map(_ => ())

  def update(id: Long, newPage: Page): Future[Unit] = {
    val pageToUpdate: Page = newPage.copy(id)
    db.run(page.filter(_.id === id).update(pageToUpdate)).map(_ => ())
  }

  class PageTable(tag: Tag) extends Table[Page](tag, "page") {

    def * = (id, content) <> ((Page.apply _).tupled, Page.unapply)

    /** The ID column, which is the primary key, and auto incremented */
    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    /** The name column */
    def content = column[String]("content")
  }

}

