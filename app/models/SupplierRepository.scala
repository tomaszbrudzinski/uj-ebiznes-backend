package models

import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class SupplierRepository @Inject() (dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  private val supplier = TableQuery[SupplierTable]

  def create(name: String, address: String): Future[Supplier] = db.run {
    (supplier.map(p => (p.name, p.address))
      returning supplier.map(_.id)
      into { case ((name, address), id) => Supplier(id, name, address) }
    ) += (name, address)
  }

  def list(): Future[Seq[Supplier]] = db.run {
    supplier.result
  }

  def getById(id: Long): Future[Supplier] = db.run {
    supplier.filter(_.id === id).result.head
  }

  def getByIdOption(id: Long): Future[Option[Supplier]] = db.run {
    supplier.filter(_.id === id).result.headOption
  }

  def delete(id: Long): Future[Unit] = db.run(supplier.filter(_.id === id).delete).map(_ => ())

  def update(id: Long, newSupplier: Supplier): Future[Unit] = {
    val supplierToUpdate: Supplier = newSupplier.copy(id)
    db.run(supplier.filter(_.id === id).update(supplierToUpdate)).map(_ => ())
  }

  private class SupplierTable(tag: Tag) extends Table[Supplier](tag, "supplier") {

    def * = (id, name, address) <> ((Supplier.apply _).tupled, Supplier.unapply)

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def name = column[String]("name")

    def address = column[String]("address")

  }

}

