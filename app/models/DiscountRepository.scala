package models

import javax.inject.{Inject, Singleton}
import play.api.db.slick.DatabaseConfigProvider
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class DiscountRepository @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) {
  val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  val discount = TableQuery[DiscountTable]

  def create(value: Int, active: Int): Future[Discount] = db.run {
    (discount.map(d => (d.value, d.active))
      returning discount.map(_.id)
      into { case ((value, active), id) => Discount(id, value, active) }
    ) += (value, active)
  }

  def list(): Future[Seq[Discount]] = db.run {
    discount.result
  }

  def getById(id: Long): Future[Discount] = db.run {
    discount.filter(_.id === id).result.head
  }

  def getByIdOption(id: Long): Future[Option[Discount]] = db.run {
    discount.filter(_.id === id).result.headOption
  }

  def delete(id: Long): Future[Unit] = db.run(discount.filter(_.id === id).delete).map(_ => ())

  def update(id: Long, newDiscount: Discount): Future[Unit] = {
    val discountToUpdate: Discount = newDiscount.copy(id)
    db.run(discount.filter(_.id === id).update(discountToUpdate)).map(_ => ())
  }

  class DiscountTable(tag: Tag) extends Table[Discount](tag, "discount") {

    def * = (id, value, active) <> ((Discount.apply _).tupled, Discount.unapply)

    def id = column[Long]("id", O.PrimaryKey, O.AutoInc)

    def value = column[Int]("value")

    def active = column[Int]("active")
  }

}

