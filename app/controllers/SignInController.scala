package controllers

import com.mohiva.play.silhouette.api.exceptions.ProviderException
import com.mohiva.play.silhouette.api.util.Credentials
import com.mohiva.play.silhouette.impl.exceptions.IdentityNotFoundException
import forms.SignInForm
import javax.inject.Inject
import play.api.i18n.Messages
import play.api.mvc.{Action, AnyContent, Request}
import utils.route.Calls

import scala.concurrent.{ExecutionContext, Future}

/**
 * The `Sign In` controller.
 */
class SignInController @Inject()(
  scc: SilhouetteControllerComponents,
  signIn: views.html.signIn,
)(implicit ex: ExecutionContext) extends AbstractAuthController(scc) {

  /**
   * Views the `Sign In` page.
   *
   * @return The result to display.
   */
  def view: Action[AnyContent] = unsecuredAction.async { implicit request: Request[AnyContent] =>
    Future.successful(Ok(signIn(SignInForm.form, socialProviderRegistry)))
  }

  /**
   * Handles the submitted form.
   *
   * @return The result to display.
   */
  def submit: Action[AnyContent] = unsecuredAction.async { implicit request: Request[AnyContent] =>
    SignInForm.form.bindFromRequest.fold(
      form => Future.successful(BadRequest(signIn(form, socialProviderRegistry))),
      data => {
        val credentials = Credentials(data.email, data.password)
        credentialsProvider.authenticate(credentials).flatMap { loginInfo =>
          userService.retrieve(loginInfo).flatMap {
            case Some(user) =>
              authenticateUser(user, data.rememberMe)
            case None => Future.failed(new IdentityNotFoundException("Couldn't find user"))
          }
        }.recover {
          case _: ProviderException =>
            Redirect(Calls.signin).flashing("error" -> Messages("invalid.credentials"))
        }
      }
    )
  }
}
