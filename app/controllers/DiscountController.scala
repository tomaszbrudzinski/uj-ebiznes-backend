package controllers

import javax.inject._
import play.api.mvc._

@Singleton
class DiscountController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  def getDiscountsList() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def getCurrentDiscount() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def addDiscount() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def addDiscountHandle() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def editDiscount(id: Long) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def editDiscountHandle() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def deleteDiscount(id: Long) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

}
