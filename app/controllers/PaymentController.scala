package controllers

import forms.{PaymentAddForm, PaymentEditForm}
import javax.inject._
import models.services.UserService
import models.{Payment, PaymentRepository}
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._

import scala.language.postfixOps
import scala.concurrent.duration._

import scala.concurrent.{Await, ExecutionContext, Future}

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class PaymentController @Inject()(
                                   paymentsRepo: PaymentRepository,
                                   userService: UserService,
                                   scc: SilhouetteControllerComponents,
                                 )(implicit ec: ExecutionContext) extends SilhouetteController(scc) {

  val paymentForm: Form[PaymentAddForm] = PaymentAddForm.form

  val updatePaymentForm: Form[PaymentEditForm] = PaymentEditForm.form

  def getPaymentsList: Action[AnyContent] = Action.async { implicit request =>
    val payments = paymentsRepo.list()
    payments.map(payments => Ok(views.html.payments(payments)))
  }

  def getPayment(id: Long): Action[AnyContent] = Action.async { implicit request =>
    val payment = paymentsRepo.getByIdOption(id)
    payment.map {
      case Some(p) => Ok(views.html.payment(p))
      case None => Redirect(routes.PaymentController.getPaymentsList())
    }
  }

  def addPayment(): Action[AnyContent] = Action.async { implicit request: MessagesRequest[AnyContent] =>
    val users = userService.list()
    users.map(_ => Ok(views.html.paymentadd(paymentForm)))
  }

  def addPaymentHandle(): Action[AnyContent] = Action.async { implicit request =>

    paymentForm.bindFromRequest.fold(
      errorForm => {
        Future.successful(
          BadRequest(views.html.paymentadd(errorForm))
        )
      },
      payment => {
        paymentsRepo.create(payment.amount, payment.accountNumber).map { _ =>
          Redirect(routes.PaymentController.addPayment()).flashing("success" -> "payment.created")
        }
      }
    )
  }

  def editPayment(id: Long): Action[AnyContent] = Action.async { implicit request: MessagesRequest[AnyContent] =>
    val payment = paymentsRepo.getById(id)
    payment.map(payment => {
      val prodForm = updatePaymentForm.fill(PaymentEditForm(payment.id, payment.amount, payment.accountNumber))
      Ok(views.html.paymentedit(prodForm))
    })
  }

  def editPaymentHandle(): Action[AnyContent] = Action.async { implicit request =>

    updatePaymentForm.bindFromRequest.fold(
      errorForm => {
        Future.successful(
          BadRequest(views.html.paymentedit(errorForm))
        )
      },
      payment => {
        paymentsRepo.update(payment.id, Payment(payment.id, payment.amount, payment.accountNumber)).map { _ =>
          Redirect(routes.PaymentController.editPayment(payment.id)).flashing("success" -> "payment updated")
        }
      }
    )
  }

  def deletePayment(id: Long): Action[AnyContent] = Action {
    paymentsRepo.delete(id)
    Redirect("/payments")
  }

  def getPaymentsApi: Action[AnyContent] = securedAction.async { implicit request =>
    val payments = paymentsRepo.list()
    payments.map(payments => Ok(Json.toJson(payments)))
  }

  def addPaymentApi(): Action[AnyContent] = securedAction { implicit request =>
    val payment: Payment = request.body.asJson.get.as[Payment]
    val paymentResponse = Await.result(paymentsRepo.create(payment.amount, payment.accountNumber), 10 second)
    Ok(Json.toJson(paymentResponse))
  }

  def editPaymentApi(): Action[AnyContent] = securedAction { implicit request =>
    val payment: Payment = request.body.asJson.get.as[Payment]
    paymentsRepo.update(payment.id, Payment(payment.id, payment.amount, payment.accountNumber))
    Ok
  }
}
