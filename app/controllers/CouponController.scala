package controllers

import javax.inject._
import play.api.mvc._

@Singleton
class CouponController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  def getCouponsList() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def getCoupon(code: String) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def addCoupon() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def addCouponHandle() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def editCoupon(id: Long) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def editCouponHandle() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def deleteCoupon(id: Long) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

}
