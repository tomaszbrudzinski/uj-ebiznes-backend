package controllers

import forms.{OrderAddForm, OrderEditForm}
import javax.inject._
import models.services.UserService
import models.{Cart, CartRepository, Order, OrderRepository, Payment, PaymentRepository, User}
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.{Failure, Success}

@Singleton
class OrderController @Inject()(val scc: SilhouetteControllerComponents,
                                orderRepository: OrderRepository,
                                cartRepository: CartRepository,
                                paymentRepository: PaymentRepository,
                                userService: UserService
                               )(implicit ec: ExecutionContext)  extends SilhouetteController(scc) {

  val orderAddForm: Form[OrderAddForm] = OrderAddForm.form

  val orderEditForm: Form[OrderEditForm] = OrderEditForm.form

  def getOrdersList: Action[AnyContent] = Action.async { implicit request =>
    val orders = orderRepository.list()
    orders.map(orders => Ok(views.html.orders(orders)))
  }

  def getOrder(id: Long): Action[AnyContent] = Action.async { implicit request =>
    val order = orderRepository.getByIdOption(id)
    order.map {
      case Some(p) => Ok(views.html.order(p))
      case None => Redirect(routes.OrderController.getOrdersList())
    }
  }

  def addOrder(): Action[AnyContent] = Action { implicit request: MessagesRequest[AnyContent] =>
    var us: Seq[User] = Seq[User]()
    userService.list().onComplete {
      case Success(u) => us = u
      case Failure(_) => print("fail")
    }

    var car: Seq[Cart] = Seq[Cart]()
    cartRepository.list().onComplete {
      case Success(ca) => car = ca
      case Failure(_) => print("fail")
    }

    var paym: Seq[Payment] = Seq[Payment]()
    paymentRepository.list().onComplete {
      case Success(pa) => paym = pa
      case Failure(_) => print("fail")
    }
    Ok(views.html.orderadd(orderAddForm, us, car, paym))
  }

  def addOrderHandle(): Action[AnyContent] = Action.async { implicit request =>
    var us: Seq[User] = Seq[User]()
    userService.list().onComplete {
      case Success(u) => us = u
      case Failure(_) => print("fail")
    }

    var car: Seq[Cart] = Seq[Cart]()
    cartRepository.list().onComplete {
      case Success(ca) => car = ca
      case Failure(_) => print("fail")
    }

    var paym: Seq[Payment] = Seq[Payment]()
    paymentRepository.list().onComplete {
      case Success(pa) => paym = pa
      case Failure(_) => print("fail")
    }

    orderAddForm.bindFromRequest.fold(
      errorForm => {
        Future.successful(
          BadRequest(views.html.orderadd(errorForm, us, car, paym))
        )
      },
      order => {
        orderRepository.create(order.user, order.cart, order.payment).map { _ =>
          Redirect(routes.OrderController.addOrder()).flashing("success" -> "order.created")
        }
      }
    )
  }

  def editOrder(id: Long): Action[AnyContent] = Action.async { implicit request: MessagesRequest[AnyContent] =>
    var us: Seq[User] = Seq[User]()
    userService.list().onComplete {
      case Success(u) => us = u
      case Failure(_) => print("fail")
    }

    var car: Seq[Cart] = Seq[Cart]()
    cartRepository.list().onComplete {
      case Success(ca) => car = ca
      case Failure(_) => print("fail")
    }

    var paym: Seq[Payment] = Seq[Payment]()
    paymentRepository.list().onComplete {
      case Success(pa) => paym = pa
      case Failure(_) => print("fail")
    }

    val order = orderRepository.getById(id)
    order.map(order => {
      val orderForm = orderEditForm.fill(OrderEditForm(order.id, order.user, order.cart, order.payment))
      Ok(views.html.orderedit(orderForm, us, car, paym))
    })
  }

  def editOrderHandle(): Action[AnyContent] = Action.async { implicit request =>
    var us: Seq[User] = Seq[User]()
    userService.list().onComplete {
      case Success(u) => us = u
      case Failure(_) => print("fail")
    }

    var car: Seq[Cart] = Seq[Cart]()
    cartRepository.list().onComplete {
      case Success(ca) => car = ca
      case Failure(_) => print("fail")
    }

    var paym: Seq[Payment] = Seq[Payment]()
    paymentRepository.list().onComplete {
      case Success(pa) => paym = pa
      case Failure(_) => print("fail")
    }

    orderEditForm.bindFromRequest.fold(
      errorForm => {
        Future.successful(
          BadRequest(views.html.orderedit(errorForm, us, car, paym))
        )
      },
      order => {
        orderRepository.update(order.id, Order(order.id, order.user, order.cart, order.payment)).map { _ =>
          Redirect(routes.OrderController.editOrder(order.id)).flashing("success" -> "order updated")
        }
      }
    )
  }

  def deleteOrder(id: Long): Action[AnyContent] = Action {
    orderRepository.delete(id)
    Redirect("/orders")
  }

  def getOrderForUserApi: Action[AnyContent] = securedAction.async { implicit request =>
    val orders = orderRepository.getByUser(request.identity.userID.toString)
    orders.map(order => Ok(Json.toJson(order)))
  }

  def addOrderApi(): Action[AnyContent] = securedAction.async { implicit request =>
    val order: Order = request.body.asJson.get.as[Order]
    var carts: Seq[Cart] = Seq[Cart]()
    carts = Await.result(cartRepository.getByUser(request.identity.userID.toString), 10 second)
    carts.foreach(
      cart => Await.result(orderRepository
        .create(request.identity.userID.toString, cart.id, order.payment), 10 second))
    val orders = orderRepository.getByUser(request.identity.userID.toString)
    orders.map(order => Ok(Json.toJson(order)))
  }

  def editOrderApi(): Action[AnyContent] = securedAction { implicit request =>
    val order: Order = request.body.asJson.get.as[Order]
    orderRepository.update(order.id, Order(order.id, order.user, order.cart, order.payment))
    Ok
  }

}
