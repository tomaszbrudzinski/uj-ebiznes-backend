package controllers

import javax.inject._
import play.api.mvc._

@Singleton
class PageController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  def getPagesList() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def getPage(id: Long) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def addPage() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def addPageHandle() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def editPage(id: Long) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def editPageHandle() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def deletePage(id: Long) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

}
