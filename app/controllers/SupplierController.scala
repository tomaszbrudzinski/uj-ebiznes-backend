package controllers

import javax.inject._
import play.api.mvc._

@Singleton
class SupplierController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  def getSuppliersList() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def getSupplier(id: Long) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def addSupplier() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def addSupplierHandle() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def editSupplier(id: Long) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def editSupplierHandle() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

  def deleteSupplier(id: Long) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }

}
