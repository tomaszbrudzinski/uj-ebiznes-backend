package controllers

import forms.{CategoryAddForm, CategoryEditForm}
import javax.inject._
import models.{Category, CategoryRepository}
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._

@Singleton
class CategoryController @Inject()(val scc: SilhouetteControllerComponents, categoriesRepository: CategoryRepository)(implicit ec: ExecutionContext) extends SilhouetteController(scc) {

  val categoryForm: Form[CategoryAddForm] = CategoryAddForm.form
  val updateCategoryForm: Form[CategoryEditForm] = CategoryEditForm.form

  def getCategoriesList: Action[AnyContent] = Action.async { implicit request =>
    val categories = categoriesRepository.list()
    categories.map(categories => Ok(views.html.categories(categories)))
  }

  def getCategory(id: Long): Action[AnyContent] = Action.async { implicit request =>
    val category = categoriesRepository.getByIdOption(id)
    category.map {
      case Some(p) => Ok(views.html.category(p))
      case None => Redirect(routes.CategoryController.getCategoriesList())
    }
  }

  def addCategory(): Action[AnyContent] = Action { implicit request: MessagesRequest[AnyContent] =>
    Ok(views.html.categoryadd(categoryForm))
  }

  def addCategoryHandle(): Action[AnyContent] = Action.async { implicit request =>
    categoryForm.bindFromRequest.fold(
      errorForm => {
        Future.successful(
          BadRequest(views.html.categoryadd(errorForm))
        )
      },
      category => {
        categoriesRepository.create(category.name).map { _ =>
          Redirect(routes.CategoryController.addCategory()).flashing("success" -> "category.created")
        }
      }
    )
  }

  def editCategory(id: Long): Action[AnyContent] = Action.async { implicit request: MessagesRequest[AnyContent] =>
    val category = categoriesRepository.getById(id)
    category.map(category => {
      val prodForm = updateCategoryForm.fill(CategoryEditForm(category.id, category.name))
      Ok(views.html.categoryedit(prodForm))
    })
  }

  def editCategoryHandle(): Action[AnyContent] = Action.async { implicit request =>
    updateCategoryForm.bindFromRequest.fold(
      errorForm => {
        Future.successful(
          BadRequest(views.html.categoryedit(errorForm))
        )
      },
      category => {
        categoriesRepository.update(category.id, Category(category.id, category.name)).map { _ =>
          Redirect(routes.CategoryController.editCategory(category.id)).flashing("success" -> "category updated")
        }
      }
    )
  }

  def deleteCategory(id: Long): Action[AnyContent] = Action {
    categoriesRepository.delete(id)
    Redirect("/categories")
  }

  def getCategoriesApi: Action[AnyContent] = securedAction.async { implicit request =>
    val categories = categoriesRepository.list()
    categories.map(categories => Ok(Json.toJson(categories)))
  }

  def addCategoryApi(): Action[AnyContent] = securedAction { implicit request =>
    val category: Category = request.body.asJson.get.as[Category]
    val categoryResponse = Await.result(categoriesRepository.create(category.name), 10 second)
    Ok(Json.toJson(categoryResponse))
  }

  def editCategoryApi(): Action[AnyContent] = securedAction { implicit request =>
    val category: Category = request.body.asJson.get.as[Category]
    categoriesRepository.update(category.id, Category(category.id, category.name))
    Ok
  }

}
