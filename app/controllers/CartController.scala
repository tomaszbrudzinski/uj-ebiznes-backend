package controllers

import forms.{CartAddForm, CartEditForm}
import javax.inject._
import models.{Cart, CartRepository, Product, ProductRepository, User}
import models.services.UserService
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._
import scala.concurrent.duration._

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.util.{Failure, Success}

@Singleton
class CartController @Inject()(
                                cartsRepo: CartRepository,
                                productRepo: ProductRepository,
                                userService: UserService,
                                scc: SilhouetteControllerComponents,
                              )(implicit ec: ExecutionContext) extends SilhouetteController(scc) {
  val cartForm: Form[CartAddForm] = CartAddForm.form

  val updateCartForm: Form[CartEditForm] = CartEditForm.form

  def getCartsList: Action[AnyContent] = Action.async { implicit request =>
    val carts = cartsRepo.list()
    carts.map(carts => Ok(views.html.carts(carts)))
  }

  def getCart(id: Long): Action[AnyContent] = Action.async { implicit request =>
    val cart = cartsRepo.getByIdOption(id)
    cart.map {
      case Some(p) => Ok(views.html.cart(p))
      case None => Redirect(routes.CartController.getCartsList())
    }
  }

  def addCart(): Action[AnyContent] = Action { implicit request: MessagesRequest[AnyContent] =>
    var prod: Seq[Product] = Seq[Product]()
    productRepo.list().onComplete {
      case Success(pr) => prod = pr
      case Failure(_) => print("fail")
    }

    var us: Seq[User] = Seq[User]()
    userService.list().onComplete {
      case Success(u) => us = u
      case Failure(_) => print("fail")
    }
    Ok(views.html.cartadd(cartForm, prod, us))
  }

  def addCartHandle(): Action[AnyContent] = Action.async { implicit request =>
    var prod: Seq[Product] = Seq[Product]()
    productRepo.list().onComplete {
      case Success(pr) => prod = pr
      case Failure(_) => print("fail")
    }

    var us: Seq[User] = Seq[User]()
    userService.list().onComplete {
      case Success(u) => us = u
      case Failure(_) => print("fail")
    }

    cartForm.bindFromRequest.fold(
      errorForm => {
        Future.successful(
          BadRequest(views.html.cartadd(errorForm, prod, us))
        )
      },
      cart => {
        cartsRepo.create(cart.product, cart.user, 0).map { _ =>
          Redirect(routes.CartController.addCart()).flashing("success" -> "cart.created")
        }
      }
    )
  }

  def editCart(id: Long): Action[AnyContent] = Action.async { implicit request: MessagesRequest[AnyContent] =>
    var prod: Seq[Product] = Seq[Product]()
    productRepo.list().onComplete {
      case Success(pr) => prod = pr
      case Failure(_) => print("fail")
    }

    var us: Seq[User] = Seq[User]()
    userService.list().onComplete {
      case Success(u) => us = u
      case Failure(_) => print("fail")
    }

    val cart = cartsRepo.getById(id)
    cart.map(cart => {
      val cartForm = updateCartForm.fill(CartEditForm(cart.id, cart.product, cart.user))
      Ok(views.html.cartedit(cartForm, prod, us))
    })
  }

  def editCartHandle(): Action[AnyContent] = Action.async { implicit request =>
    var prod: Seq[Product] = Seq[Product]()
    productRepo.list().onComplete {
      case Success(pr) => prod = pr
      case Failure(_) => print("fail")
    }

    var us: Seq[User] = Seq[User]()
    userService.list().onComplete {
      case Success(u) => us = u
      case Failure(_) => print("fail")
    }

    updateCartForm.bindFromRequest.fold(
      errorForm => {
        Future.successful(
          BadRequest(views.html.cartedit(errorForm, prod, us))
        )
      },
      cart => {
        cartsRepo.update(cart.id, Cart(cart.id, cart.product, cart.user, 0)).map { _ =>
          Redirect(routes.CartController.editCart(cart.id)).flashing("success" -> "cart updated")
        }
      }
    )
  }

  def deleteCart(id: Long): Action[AnyContent] = Action {
    cartsRepo.delete(id)
    Redirect("/carts")
  }

  def getCartsForUserApi: Action[AnyContent] = securedAction.async { implicit request =>
    val carts = cartsRepo.getByUser(request.identity.userID.toString)
    carts.map(carts => Ok(Json.toJson(carts)))
  }

  def addCartApi(): Action[AnyContent] = securedAction { implicit request =>
    val cart: Cart = request.body.asJson.get.as[Cart]
    val cartResponse = Await.result(cartsRepo.create(cart.product, request.identity.userID.toString, 0), 10 second)
    Ok(Json.toJson(cartResponse))
  }

  def editCartApi(): Action[AnyContent] = securedAction { implicit request =>
    val cart: Cart = request.body.asJson.get.as[Cart]
    cartsRepo.update(cart.id, Cart(cart.id, cart.product, cart.user, 0))
    Ok
  }
}
