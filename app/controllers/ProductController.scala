package controllers

import forms.{ProductAddForm, ProductEditForm}
import javax.inject._
import models.{Category, CategoryRepository, Product, ProductRepository}
import play.api.data.Form
import play.api.libs.json.Json

import scala.language.postfixOps
import play.api.mvc._

import scala.concurrent.{Await, ExecutionContext, Future}
import scala.concurrent.duration._
import scala.util.{Failure, Success}

@Singleton
class ProductController @Inject()(val scc: SilhouetteControllerComponents,
                                  productsRepository: ProductRepository,
                                  categoryRepository: CategoryRepository
                                 )(implicit exec: ExecutionContext) extends SilhouetteController(scc) {

  val addProductForm: Form[ProductAddForm] = ProductAddForm.form
  val editProductForm: Form[ProductEditForm] = ProductEditForm.form

  def getProductsList: Action[AnyContent] = Action.async { implicit request =>
    val products = productsRepository.list()
    products.map(products => Ok(views.html.products(products)))
  }

  def getProductsByCategory(id: Long): Action[AnyContent] = Action.async { implicit request =>
    val products = productsRepository.getByCategory(id)
    products.map(products => Ok(views.html.products(products)))
  }

  def getProduct(id: Long): Action[AnyContent] = Action.async { implicit request =>
    val product = productsRepository.getByIdOption(id)
    product.map {
      case Some(p) => Ok(views.html.product(p))
      case None => Redirect(routes.ProductController.getProductsList())
    }
  }

  def addProduct(): Action[AnyContent] = Action.async { implicit request: MessagesRequest[AnyContent] =>
    val categories = categoryRepository.list()
    categories.map(cat => Ok(views.html.productadd(addProductForm, cat)))
  }

  def addProductHandle(): Action[AnyContent] = Action.async { implicit request =>
    var categ: Seq[Category] = Seq[Category]()
    categoryRepository.list().onComplete {
      case Success(cat) => categ = cat
      case Failure(_) => print("failure")
    }

    addProductForm.bindFromRequest.fold(
      errorForm => {
        Future.successful(
          BadRequest(views.html.productadd(errorForm, categ))
        )
      },
      product => {
        productsRepository.create(product.name, product.description, product.category).map { _ =>
          Redirect(routes.ProductController.addProduct()).flashing("success" -> "product.created")
        }
      }
    )

  }

  def editProduct(id: Long): Action[AnyContent] = Action.async { implicit request: MessagesRequest[AnyContent] =>
    var categ: Seq[Category] = Seq[Category]()
    categoryRepository.list().onComplete {
      case Success(cat) => categ = cat
      case Failure(_) => print("fail")
    }

    val product = productsRepository.getById(id)
    product.map(product => {
      val prodForm = editProductForm.fill(ProductEditForm(product.id, product.name, product.description, product.category))
      Ok(views.html.productedit(prodForm, categ))
    })
  }

  def editProductHandle(): Action[AnyContent] = Action.async { implicit request =>
    var categ: Seq[Category] = Seq[Category]()
    categoryRepository.list().onComplete {
      case Success(cat) => categ = cat
      case Failure(_) => print("fail")
    }

    editProductForm.bindFromRequest.fold(
      errorForm => {
        Future.successful(
          BadRequest(views.html.productedit(errorForm, categ))
        )
      },
      product => {
        productsRepository.update(product.id, Product(product.id, product.name, product.description, product.category)).map { _ =>
          Redirect(routes.ProductController.editProduct(product.id)).flashing("success" -> "product updated")
        }
      }
    )
  }

  def deleteProduct(id: Long): Action[AnyContent] = Action {
    productsRepository.delete(id)
    Redirect("/products")
  }

  def getProductsApi: Action[AnyContent] = securedAction.async { implicit request =>
    val products = productsRepository.list()
    products.map(products => Ok(Json.toJson(products)))
  }


  def addProductApi(): Action[AnyContent] = securedAction { implicit request =>
    val product: Product = request.body.asJson.get.as[Product]
    val productResponse = Await.result(productsRepository.create(product.name, product.description, product.category), 10 second)
    Ok(Json.toJson(productResponse))
  }

  def editProductApi(): Action[AnyContent] = securedAction { implicit request =>
    val product: Product = request.body.asJson.get.as[Product]
    productsRepository.update(product.id, Product(product.id, product.name, product.description, product.category))
    Ok
  }

}
