package forms

import play.api.data.Form
import play.api.data.Forms._

object ProductEditForm {

  val form: Form[ProductEditForm] = Form[ProductEditForm] {
    mapping(
      "id" -> longNumber,
      "name" -> nonEmptyText,
      "description" -> nonEmptyText,
      "category" -> longNumber,
    )(ProductEditForm.apply)(ProductEditForm.unapply)
  }
}

case class ProductEditForm(id: Long, name: String, description: String, category: Long)
