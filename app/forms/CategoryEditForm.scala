package forms

import play.api.data.Form
import play.api.data.Forms._

object CategoryEditForm {

  val form: Form[CategoryEditForm] = Form[CategoryEditForm] {
    mapping(
      "id" -> longNumber,
      "name" -> nonEmptyText,
    )(CategoryEditForm.apply)(CategoryEditForm.unapply)
  }
}

case class CategoryEditForm(id: Long, name: String)
