package forms

import play.api.data.Form
import play.api.data.Forms._

object PaymentEditForm {

  val form: Form[PaymentEditForm] = Form[PaymentEditForm] {
    mapping(
      "id" -> longNumber,
      "amount" -> number,
      "accountNumber" -> nonEmptyText,
    )(PaymentEditForm.apply)(PaymentEditForm.unapply)
  }
}

case class PaymentEditForm(id: Long, amount: Int, accountNumber: String)
