package forms

import play.api.data.Form
import play.api.data.Forms._

object OrderEditForm {

  val form: Form[OrderEditForm] = Form[OrderEditForm] {
    mapping(
      "id" -> longNumber,
      "user" -> text,
      "cart" -> longNumber,
      "payment" -> longNumber,
    )(OrderEditForm.apply)(OrderEditForm.unapply)
  }
}

case class OrderEditForm(id: Long, user: String, cart: Long, payment: Long)
