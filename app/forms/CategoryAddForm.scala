package forms

import play.api.data.Form
import play.api.data.Forms._

object CategoryAddForm {

  val form: Form[CategoryAddForm] = Form[CategoryAddForm] {
    mapping(
      "name" -> text,
    )(CategoryAddForm.apply) (CategoryAddForm.unapply)
  }
}

case class CategoryAddForm(name: String)
