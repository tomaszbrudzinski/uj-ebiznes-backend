package forms

import play.api.data.Form
import play.api.data.Forms._

object CartEditForm {

  val form: Form[CartEditForm] = Form[CartEditForm] {
    mapping(
      "id" -> longNumber,
      "product" -> longNumber,
      "user" -> text,
    )(CartEditForm.apply)(CartEditForm.unapply)
  }
}

case class CartEditForm(id: Long, product: Long, user: String)