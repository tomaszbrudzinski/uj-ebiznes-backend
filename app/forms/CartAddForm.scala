package forms

import play.api.data.Form
import play.api.data.Forms._

object CartAddForm {

  val form: Form[CartAddForm] = Form[CartAddForm] {
    mapping(
      "product" -> longNumber,
      "user" -> text,
    )(CartAddForm.apply) (CartAddForm.unapply)
  }
}

case class CartAddForm(product: Long, user: String)