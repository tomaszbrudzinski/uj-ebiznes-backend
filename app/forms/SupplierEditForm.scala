package forms

import play.api.data.Form
import play.api.data.Forms._

object SupplierEditForm {

  val form: Form[SupplierEditForm] = Form[SupplierEditForm] {
    mapping(
      "id" -> longNumber,
      "name" -> nonEmptyText,
      "address" -> nonEmptyText,
    )(SupplierEditForm.apply)(SupplierEditForm.unapply)
  }
}

case class SupplierEditForm(id: Long, name: String, address: String)

