package forms

import play.api.data.Form
import play.api.data.Forms._

object PaymentAddForm {

  val form: Form[PaymentAddForm] = Form[PaymentAddForm] {
    mapping(
      "amount" -> number,
      "accountNumber" -> nonEmptyText,
    )(PaymentAddForm.apply)(PaymentAddForm.unapply)
  }
}

case class PaymentAddForm(amount: Int, accountNumber: String)
