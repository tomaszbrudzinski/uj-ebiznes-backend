package forms

import play.api.data.Form
import play.api.data.Forms._

object OrderAddForm {

  val form: Form[OrderAddForm] = Form[OrderAddForm] {
    mapping(
      "user" -> text,
      "cart" -> longNumber,
      "payment" -> longNumber,
    )(OrderAddForm.apply)(OrderAddForm.unapply)
  }
}
case class OrderAddForm(user: String, cart: Long, payment: Long)
