package forms

import play.api.data.Form
import play.api.data.Forms._

object SupplierAddForm {

  val form: Form[SupplierAddForm] = Form[SupplierAddForm] {
    mapping(
      "name" -> nonEmptyText,
      "address" -> nonEmptyText,
    )(SupplierAddForm.apply)(SupplierAddForm.unapply)
  }
}

case class SupplierAddForm(name: String, address: String)
