package forms

import play.api.data.Form
import play.api.data.Forms._

object ProductAddForm {

  val form: Form[ProductAddForm] = Form[ProductAddForm] {
    mapping(
      "name" -> nonEmptyText,
      "description" -> nonEmptyText,
      "category" -> longNumber,
    )(ProductAddForm.apply)(ProductAddForm.unapply)
  }
}

case class ProductAddForm(name: String, description: String, category: Long)
